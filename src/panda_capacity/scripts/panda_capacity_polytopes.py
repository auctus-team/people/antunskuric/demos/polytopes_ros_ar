#!/usr/bin/env python
import rospy
# time evaluation
import time
#some math
import numpy as np


# URDF parsing an kinematics
import pinocchio as pin

# import pycapacity robot
from pycapacity.robot import *
# reachable space utils
from reachable_space_utils import *


# polytope messages
from sensor_msgs.msg import JointState, PointCloud
from jsk_recognition_msgs.msg import PolygonArray
from geometry_msgs.msg import Polygon, Point32, PolygonStamped
from std_msgs.msg import Header


import dynamic_reconfigure.client
from panda_capacity.cfg import CapacityConfig
from dynamic_reconfigure.server import Server

polytope_type = "force"
options = {
    'default_scaling': True,
    'scaling_factor': 1000.0,
    'horizon_time': 0.2,
}
def callback_server(config, level):
    global polytope_type, options
    rospy.loginfo("""Reconfigure Request: Polytope: {polytope}""".format(**config))
    polytope_type = config["polytope"]
    options['default_scaling'] = config["default_scaling"]
    options['scaling_factor'] = config["scaling_factor"]
    options['horizon_time'] = float(config["horizon_time_ms"])/1000.0
    return config


# visualisation of vertices
def create_vertex_msg(force_vertex, pose, frame, scaling_factor = 500):
    pointcloud_massage = PointCloud()
    for i in range(force_vertex.shape[1]):
        point = Point32()
        point.x = force_vertex[0,i]/scaling_factor + pose[0]
        point.y = force_vertex[1,i]/scaling_factor + pose[1]
        point.z = force_vertex[2,i]/scaling_factor + pose[2]
        pointcloud_massage.points.append(point)

    # polytop stamped message
    pointcloud_massage.header = Header()
    pointcloud_massage.header.frame_id = frame
    pointcloud_massage.header.stamp = rospy.Time.now()
    return pointcloud_massage


# visualisation of polytope faces
def create_polytopes_msg(force_polytopes, pose, frame, scaling_factor = 500):
    polygonarray_message = PolygonArray()
    polygonarray_message.header = Header()
    polygonarray_message.header.frame_id = frame
    polygonarray_message.header.stamp = rospy.Time.now()
    for face_polygon in force_polytopes:
        polygon_massage = Polygon()
        for i in range(face_polygon.shape[1]):
            point = Point32()
            point.x = face_polygon[0,i]/scaling_factor + pose[0]
            point.y = face_polygon[1,i]/scaling_factor + pose[1]
            point.z = face_polygon[2,i]/scaling_factor + pose[2]
            polygon_massage.points.append(point)

        # polytope stamped message
        polygon_stamped = PolygonStamped()
        polygon_stamped.polygon = polygon_massage
        polygon_stamped.header = Header()
        polygon_stamped.header.frame_id = frame
        polygon_stamped.header.stamp = rospy.Time.now()
        polygonarray_message.polygons.append(polygon_stamped)
        polygonarray_message.likelihood.append(1.0)
    return polygonarray_message


# initial joint positions
joint_positions = [0,0,0,0,0,0,0]

# function receiveing the new joint positions
def callback(data):
    global joint_positions
    joint_positions = np.array(data.position)


def plot_polytope(robot, q, frame_name = None):
    global polytope_type, options

    # if no joint state received, return
    if not np.sum(q):
        return

    # if frame not specified, use the last frame
    if frame_name is None:
        frame_name = robot.model.frames[-1].name

    # calculate forward kinematics of the robot
    robot.forwardKinematics(q)
    ee_position = robot.data.oMf[robot.model.getFrameId(frame_name)].translation

    # calculate jacobi matrix
    robot.computeJointJacobians(q)
    J = pin.getFrameJacobian(robot.model, robot.data, robot.model.getFrameId(frame_name) , pin.ReferenceFrame.LOCAL_WORLD_ALIGNED)
    pin.crba(robot.model,robot.data,np.array(q))
    M = np.array(robot.data.M)
    # only position part
    J = J[:3,:]

    # maximal joint angles
    q_max = robot.model.upperPositionLimit.T
    q_min = robot.model.lowerPositionLimit.T
    # maximal joint velocities
    dq_max = robot.model.velocityLimit.T
    dq_min = -dq_max
    # maximal joint torques
    t_max = robot.model.effortLimit.T
    t_min = -t_max
    # maximal joint jerks
    dddq_max = np.array([7500.0,3750.0,5000.0,6250.0,7500.0,10000.0,10000]).T  
    dddq_min = -dddq_max


    # decide which polytope to calculate
    if "force" in polytope_type:
        if options['default_scaling']:
            scaling_factor = 1000
        else:
            scaling_factor = options['scaling_factor']
        # calculate force vertexes
        start = time.time()
        poly = force_polytope(J, t_max, t_min)
        print(time.time() - start)
    elif "velocity" in polytope_type:
        if options['default_scaling']:
            scaling_factor = 10
        else:
            scaling_factor = options['scaling_factor']
        # calculate force vertexes
        start = time.time()
        poly = velocity_polytope(J, dq_max, dq_min)
        print(time.time() - start)
    elif "acceleration" in polytope_type:
        if options['default_scaling']:
            scaling_factor = 500
        else:
            scaling_factor = options['scaling_factor']
        # calculate force vertexes
        start = time.time()
        poly = acceleration_polytope(J, M, t_max, t_min)
        print(time.time() - start)
    elif "jerk" in polytope_type: 
        if options['default_scaling']:
            scaling_factor = 20000
        else:
            scaling_factor = options['scaling_factor']
        # calculate force vertexes
        start = time.time()
        poly = velocity_polytope(J, dddq_max, dddq_min)
        print(time.time() - start)
    elif "reachable_space_convex" in polytope_type: 
        scaling_factor = 1
        horizon_time = options['horizon_time']
        # calculate force vertexes
        start = time.time()
        poly = reachable_space_approximation(J=J, M=M, q0=q, t_max=t_max, t_min=t_min, dq_max=dq_max, dq_min=dq_min, q_min=q_min, q_max=q_max, horizon=horizon_time)
        print(time.time() - start)
    elif "reachable_space_nonconvex" in polytope_type:
        scaling_factor = 1
        horizon_time = options['horizon_time']
        # calculate the reachable space
        params = {
            'convex' : False,       # approximate the reachable space in a convex form 
            'frame' :  frame_name,   # which frame to calculate the 
            'n_samples' : 3,        # number of samples per axis (the higher the better approximation - but longer the execution time)
            'facet_dim' : 1,        # from 1 to number of robot's DOF (the higher the better approximation - but longer the execution time)
        }
        # uniform sampling of the joint space, number of samples per axis (the higher the better approximation - but longer the execution time)
        # overall number of samples will be n_samples^facet_dim
        # from 1 to number of robot's DOF (the higher the better approximation - but longer the execution time)
        if horizon_time < 0.2:
            params['n_samples'] = 3 
            params['facet_dim'] = 1 
        elif horizon_time < 0.5:
            params['n_samples'] = 3 
            params['facet_dim'] = 2 
        elif horizon_time < 0.9:
            params['n_samples'] = 4
            params['facet_dim'] = 3
        else:
            params['n_samples'] = 4
            params['facet_dim'] = 4

        start = time.time()
        poly = pycapacity.objects.Polytope()
        verts, faces = curves_reachable_set_dq_nd(robot, q, horizon_time, params)
        poly.vertices = verts.T
        poly.face_indices = faces
        print(time.time() - start)
    else: 
        print("Error: unknown polytope type")
        return

    poly.find_faces()
    # publish plytope
    publish_polytope = rospy.Publisher('polytope', PolygonArray, queue_size=10)
    publish_polytope.publish(create_polytopes_msg(poly.faces, ee_position, "world", scaling_factor))


# main function of the class
def vel_polytope():
    global joint_positions, client
    rospy.init_node('capacity_polytopes')

    # loading the root urdf from robot_description parameter
    robot = pin.RobotWrapper(model=pin.buildModelFromXML(rospy.get_param("robot_description")))


    # global variables
    server = Server(CapacityConfig, callback_server)
    client = dynamic_reconfigure.client.Client("panda_capacity_polytopes", timeout=10, config_callback=None)

    # joint state topic
    rospy.Subscriber('joint_states', JointState, callback, queue_size= 2)

    rate = rospy.Rate(15) #Hz
    while not rospy.is_shutdown():
        plot_polytope(robot, joint_positions, frame_name="panda_link8")
        rate.sleep()


# class definition
if __name__ == '__main__':
    try:
        vel_polytope()
    except rospy.ROSInterruptException:
        pass